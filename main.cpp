#include "blc_core.h"
#include "blc_channel.h"
#include "blc_program.h"
#include <unistd.h>
#include <termios.h>
#include <libgen.h> //basename


int file_get_lines_number(FILE* file){
	int ch, lines_nb;

	lines_nb=0;
	while(feof(file)==0){
	  ch = fgetc(file);

	  if (ch == '\n') lines_nb++;
	  else if ( ch == EOF ) if (feof(file)==0) EXIT_ON_SYSTEM_ERROR("Reading file");
	}
	return lines_nb;
}

void array_def_with_tsv_file_first_line(blc_array *array, char const *filename){
	char const *ext;
	FILE *file;
	size_t linecap=0;
	char *line;
	int length, lines_nb;
	char const *pos;
    ext=blc_get_filename_extension(filename);
    if (strcmp(ext, "tsv")!=0)  EXIT_ON_ERROR("'%s' does not .tsv extension but '%s'", filename, ext);
    SYSTEM_ERROR_CHECK(file=fopen(filename,"r"), NULL, "opening '%s'", filename);
    SYSTEM_ERROR_CHECK(getline(&line, &linecap, file), -1, "Reading '%s'", filename);

    pos=line;
    length=0;
    while(pos){
    	pos=strchr(pos+1, '\t');
    	if (pos) length++;
    }

    array->def_array('FL32', 'NDEF', 1, length);
    fclose(file);
}

//We suppose they are float and only one dim
void array_def_with_tsv_file(blc_array *array, char const *filename){
	char const *ext;
	FILE *file;
	size_t linecap=0;
	char *line;
	int length, lines_nb;
	char const *pos;
    ext=blc_get_filename_extension(filename);
    if (strcmp(ext, "tsv")!=0)  EXIT_ON_ERROR("'%s' does not .tsv extension but '%s'", filename, ext);
    SYSTEM_ERROR_CHECK(file=fopen(filename,"r"), NULL, "opening '%s'", filename);
    SYSTEM_ERROR_CHECK(getline(&line, &linecap, file), -1, "Reading '%s'", filename);

    pos=line;
    length=0;
    while(pos){
    	pos=strchr(pos+1, '\t');
    	if (pos)length++;
    }
    lines_nb=file_get_lines_number(file)+1; //The first line was alreay read

    array->def_array('FL32', 'NDEF', 2, length, lines_nb);
    fclose(file);
}


int main(int argc, char** argv){
    char *default_output;
    char const *extension;
    char const *display, *period_str, *text;
    char const *channel_name, *type_str,  *str_quitting_key,  *str_neutral_key, *str_active_value,  *str_neutral_value, *toggle_mode, *step_size_str, *filename;
    char const *key_nb_str;
    char *line=NULL;
    size_t linecap=0;
    ssize_t line_size;
    blc_channel channel;
    FILE *file;

    int i, period;
    char *pos;
    uchar answer;
    
    blc_program_set_description("Load tsv file of 2 dimensions");
    blc_program_add_option(&period_str, 'p', "period", "integer", "Period in ms to read line by line", "0");
    blc_program_add_option(&channel_name, 'o', "output_channel", "string", "Name of the channel to output the data", NULL);
    blc_program_add_option(&text, 't', "text", NULL, "Export the result as text instead of blc_channel", NULL);
    blc_program_add_parameter(&filename, "file name", 1, "file to load", NULL);
    blc_program_init(&argc, &argv, NULL);
    
    period=strtod(period_str, NULL);
    if (text==NULL){
    	if (channel_name==NULL) if (channel_name==NULL) asprintf((char**)&channel_name, "/%s%d", basename(argv[0]), getpid()); //This will not be free but it is only allocate once
    	if (period == 0){
    		array_def_with_tsv_file(&channel, filename);
    		channel.create_or_open(channel_name, BLC_CHANNEL_WRITE);
    		channel.update_with_tsv_file(filename);
    	}
    	else{
    		array_def_with_tsv_file_first_line(&channel, filename);
    		channel.create_or_open(channel_name, BLC_CHANNEL_WRITE);
    	}
		channel.publish();

    }
    else if (period==0) EXIT_ON_ERROR("You are in text mode and period is 0. Use the POSIX executable 'cat' for the same result");

    if (period != 0) {
    	SYSTEM_ERROR_CHECK(file=fopen(filename, "r"), NULL, "Loading '%s'",  filename);
    	BLC_COMMAND_LOOP(period*1000){
    		if (text){
    			line_size=getline(&line, &linecap, file);
    			if (line_size==-1) EXIT_ON_SYSTEM_ERROR("Reading '%s'", filename);
    			SYSTEM_SUCCESS_CHECK(write(STDOUT_FILENO, line, line_size), line_size, "Error writing on stdout");
    		}
    		else fscan_tsv_floats(file, channel.floats, channel.dims[0].length);
    	}
    }

    if (line) {
    	FREE(line);
    	linecap=0;
    }

    return EXIT_SUCCESS;
}

